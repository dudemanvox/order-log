<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user.
echo "<html>\n"
. "<head>\n"
. "<meta charset='utf-8'>\n"
. "<script src='/scripts/customer.js' type='text/javascript' ></script>\n"
. "<link rel='stylesheet' href='http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css' />\n"
. "<script src='http://code.jquery.com/jquery-1.9.1.js'></script>\n"
. "<script src='http://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>\n"
. "<script src='notes.js' type='text/javascript' ></script>\n"
. "<script src='/scripts/passconf.js' type='text/javascript'></script>\n"
. "<link rel='stylesheet' type='text/css' href='css/customer.css'>\n"
. "</head>\n"
. "<body>\n"
. "<div id='menu' name='menu' style='width: 40%;margin-left: auto; margin-right: auto;'>\n"
. "Hello " . htmlentities($_SESSION['user']['firstname'], ENT_QUOTES, 'UTF-8') 
. " " . htmlentities($_SESSION['user']['lastname'], ENT_QUOTES, 'UTF-8') 
. "! <span style='float: right;'><a href='log.php'>Order Log</a>";
if($_SESSION['user']['id'] <= '10') {
	echo " | <a href='register.php'>Register User</a>";
}

echo " | <a href='addcustomer.php'>New Order</a> | <a href='logout.php'>Logout</a> | <a href='https://docs.google.com/a/austinrockgym.com/document/d/1mfAi2mUPYIeDnnm8tNbTgMkxEUUAix5UODjXVlEpOn0/edit'>Help</a></span></div>";