<?php 
	require("menu.php");
	print "<script src='scripts/delconf.js' type='text/javascript'></script>";
	print "<link rel='stylesheet' type='text/css' href='css/log.css' />";
	function generate_log($db) {
    	print "<div id='main' name='main' style='width: 60%;margin-left: auto; margin-right: auto;'><fieldset><legend class='button'>Order History</legend>";
		$sql = "SELECT orders.order_id, orders.order_date, customers.firstname, customers.lastname, customers.phone, customers.email, orders.vendor, orders.item, orders.detail, orders.quantity, customers.pickup_loc, actions.complete, orders.notes from orders join customers on orders.cus_id = customers.cus_id join actions on orders.order_id = actions.order_id join users on orders.user_id = users.id order by orders.order_id desc LIMIT 0, 30 ";
		print "<table class='imagetable'><tr><th>Oder #</th><th>Order Date</th><th>Firstname</th><th>Lastname</th><th>Phone</th><th> Email</th><th>Vendor</th><th>Item</th><th>Detail</th><th>Quantity</th><th>Pickup AT</th><th>Closed?</th><th>Notes</th></tr>\n";
	    foreach($db->query($sql) as $row) {
	    	 print "<tr>";
	    	 foreach($row as $key=>$val) {
		    	if($key != 'notes' && $val) {
					print "<td>";
					if($key == 'order_id') {
						print "<form name='order_" .$val ."' action='details.php' method='post'><input type='hidden' name='order_id' value='" . $val . "' /><input type='submit' class='button' value='" . $val . "'/></form>";
						$note_num = 'note_' .$val;
						$order_num = $val;
					}
					elseif($key == 'complete' && $val == '1') {
						print "<span style='color: green;'>True</span></td>";
					}else {
						print $val . "</td>";					
					}
					
				}elseif($key == 'notes') {
						if($val){
							print "<td><center><a href='javascript:toggle(\"$note_num\");'><img src='img/view.png' width='20' height='20' /></a></center>\n";
							print "<span name='$note_num' id='$note_num' style='display: none;'><form name='update_$note_num' action='notes.php' method='post'><textarea name='note' rows='4' cols='30'>$val</textarea>\n";
							print "<input type='hidden' value='$order_num' name='order'><input type='submit' class='button' value='Update Notes' /></form> </span></td>";
						}else {
							print "<td><center><a href='javascript:toggle(\"$note_num\");'><img src='img/plus.png' width='20' height='20' /></a></center>";
							print "<span name='$note_num' id='$note_num' style='display: none;'><form name='update_$note_num' action='notes.php' method='post'><textarea name='note' rows='4' cols='30'>$val</textarea>\n";
							print "<input type='hidden' value='$order_num' name='order'><input type='submit' class='button' value='Update Notes' /></form> </span></td>";
						}					
					}else {
						$val = 'False';
						print "<td><span style='color: red;'>" . $val . "</span></td>";
					}
				}
			if($_SESSION['user']['id'] <= '10') {
					print "<td><form name='del_order' action='del_order.php' method='post'><input name='del_me' type='hidden' value='del_" . $order_num ."'><input type='button' class='button' value='Delete' onClick='delX(" . $order_num . ")'></form></td>";
				}		   
		    }
			//print "</table></fieldset><br />";
		  print "</tr>";       
    }
	 generate_log($db);
    