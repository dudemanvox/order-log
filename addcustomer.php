<?php 
	require("menu.php");
?> 

<div id="top" name="top" style="width: 40%;margin-left: auto; margin-right: auto;">
<form name="newcustomer" action="process.php" method="post" onsubmit="return validateForm()">
 <fieldset>
 <legend>Order Type</legend> 
 <select name="cusType" id="cusType" onchange="cusSelect(), setGym()">
  <option name="type-select" id="type-select" value="">Select One</option>
  <option name="narg-direct" id="narg-direct" value="1">NARG</option>
  <option name="sarg-direct" id="sarg-direct" value="2">SARG</option>
  <option name="narg-special" id="narg-special" value="3">NARG Special Order</option>
  <option name="sarg-special" id="sarg-special" value="4">SARG Special Order</option>
 </select>
 </fieldset>
 </div>
 <div id="customer" name="customer" style="display: none;width: 40%;margin-left: auto;margin-right: auto;"> 
 <fieldset>
 <legend>Customer Details</legend>
   <label for="first" style="align: left;">First Name:</label>
   <input type="text" name="first" id="first" class="medium" onblur="validateName(name)" />
   <span id="firstError" class="error">You can only use letters, hyphens and apostrophes.</span><br>
   <label for="last">Last Name:</label>   
   <input type="text" name="last" id="last" class="medium" onblur="validateName(name)" />
   <span id="lastError" class="error">You can only use letters, hyphens and apostrophes.</span><br>
	<label for="phone">Phone Number:</label>   
   <input type="text" name="phone" id="phone" class="medium" onblur="validatePhone(name)" />
   <span id="phoneError" class="error">Please enter numbers only. Be sure to include the area code.</span><br>
   <label for="email">Email Address:</label>
   <input type="text" name="email" id="email" class="medium" onblur="validateEmail(name)" />
	<span id="emailError" class="error">You must enter a valid email address.</span><br>
   <label for="location">Pickup At:</label>
	<select name="location" id="location" class="medium" onblur="validateLoc(name)">
		<option value="">Location</option>		
		<option value="NARG">NARG</option>
		<option value="SARG">SARG</option>
	</select>
	<span id="locationError" class="error">You must select a location for client pick-up.</span>
 </fieldset>
 </div>
 <div id="order" name="order" style="width: 40%;margin-left: auto; margin-right: auto;">
  <fieldset>
	  <legend>Order Details</legend>
	  <label>Vendor:</label>
	  <input type="text" id="vendor" name="vendor" class="medium" onblur="validateAlpha(name)" value="Letters and spaces (ie. Five Ten)" onfocus="this.value = ''" />
	  <span id="vendorError" class="error">Please specify the vendor.</span><br>
	  <label>Item:</label>
	  <input type="text" id="item" name="item" class="medium" onblur="validateAlpha(name)" />
	  <span id="itemError" class="error">Please specify the item.</span><br>
	  <label>Details:</label>
	  <input type="text" id="details" name="details" class="medium" value="Color, Size, Etc." onblur="validateAlphaNumeric(name)" />
	  <span id="detailsError" class="error">Please supply any details.</span><br>
	  <label>Quantity:</label>
	  <input type="text" id="quantity" name="quantity" class="medium" onblur="validateNumeric(name)" />
	  <span id="quantityError" class="error">Please specify the quantity.</span><br>
	  <input type="hidden" id="date" name="date" class="medium" value="<?php echo date('Y-m-d');?>" readonly="readonly" style="text-align: center; background-color: #ccffcc;" />
	  <span id="dateError" class="error">Please enter today's date.</span>
	  <label>Price:</label>
	  <input type="text" id="price" name="price" class="medium" onblur="validatePrice(name)"/>
	  <span id="priceError" class="error">Please specify the price.</span><br>
	  <label>Payment Conf:</label>
	  <input type="text" id="conf" name="conf" class="medium" onblur="validateConf(name)" />
	  <span id="confError" class="error">Please enter the payment confirmation (eg. mc-123456).</span><br>
	  <label>Shipping:</label>
	  <input type="text" id="shipping" name="shipping" class="medium" onblur="validatePrice(name)" />
	  <span id="shippingError" class="error">Please enter the amount charged for shipping.</span><br>
  </fieldset> 
 </div>
<input type="hidden" name="user_id" value="<?php echo htmlentities($_SESSION['user']['id'], ENT_QUOTES, 'UTF-8'); ?>" />
<input type="hidden" name="dialog" value="new" />
<input type="hidden" name="cus_id" value="null" />
<div id="bottom" name="bottom" style="width: 40%; margin-left: auto; margin-right: auto;">
<input type="submit" class="button" value="Submit Order" />
</div>
</form>
</body>
</html>