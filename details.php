<?php
	require("menu.php");
	print "<link rel='stylesheet' type='text/css' href='log.css' />";
	print "<script>\n"
   . "$(function() {\n"
   . "$( '#proc_date, #arrive_date, #cus_pickup' ).datepicker({\n"
   . "dateFormat: 'yy-mm-dd',\n"
   . "showOn: 'button',\n"
   . "buttonImage: 'calendar-blue.gif',\n"
   . "buttonImageOnly: true\n"
   . "});\n"
   . "});\n"
   . "</script>\n";
	
	function generate_details($db) {
    	print "<div id='main' name='main' style='width: 100%;margin-left: auto; margin-right: auto;'><fieldset><legend>Details for order# " .$_POST['order_id']."</legend>";
			try {		
			$sql = "SELECT cfo_approve, process_date, po_num, ship_via, track_num, mngr_notice_proc, arrive_date, arrive_rcvd, contents_okay, on_location, loc_rcvd, cus_notice_arrive, cus_follow_1, cus_follow_2, cus_follow_3, cus_pickup, pickup_emp, mngr_notice_pickup, age, complete FROM `actions` WHERE order_id = " . $_POST['order_id'] . " LIMIT 0, 30 ";
			print "<form id='update' name='update' action='details.php' method='post'><table class='imagetable'><input type='hidden' name='updated' value='true' /><input type='hidden' name='order_id' value='" . $_POST['order_id'] . "' /><tr><th>CFO<br />Aprroval</th><th>Processed<br />On</th><th>PO Number<br />&nbsp;</th><th>Shipping<br />Via</th><th>Tracking<br />Number</th><th>Manager<br />Notifed</th><th>Arrival<br />Date</th><th>Received<br />By</th><th>Contents As<br />Ordered?</th><th>At Pickup?</th><th>Received By</th><th>Customer Emailed?</th><th>Follow Up #1</th><th>Follow Up #2</th><th>Follow Up #3</th><th>Picked Up</th><th>Employee</th><th>Manager Informed?</th><th>Age</th></tr>\n";	    
		    foreach($db->query($sql) as $row) {
		    	 print "<tr>";
		    	 foreach($row as $key=>$val) {
			    	switch($key) {
							case 'cfo_approve':
								print "<td><center>";
								if(!$val) {
									print "<input type='checkbox' name='" .$key. "' value='1' />";							
								}else {
									print "<span style='color: green;font-weight: bold;'>True</span>";							
								}
								print "</center></td>";
								break;
							case 'process_date':
								print "<td nowrap>";
								if(!$val || $val == '0000-00-00') {
									print "<input type='text' name='" . $key ."' id='proc_date' size='10' />";
									$start_date = '0';
								}else {
									print $val;
									$start_date = strtotime($val);
								}
								print "</td>";
								break;
							case 'po_num':
								print "<td><center>";
								if(!$val) {
									print "<input type='text' name='" . $key . "' id='po_num' size='8' />";
								}else {
									print $val;
								}
								print "</td></center>";
								break;
							case 'ship_via':
								print "<td><center>";
								if(!$val) {
									print "<select name='" . $key . "' id='ship_via'>\n"
									. "<option value=''>Select</option>\n"
									. "<option value='FedEx'>FedEx</option>\n"
									. "<option value='UPS'>UPS</option>\n"
									. "<option value='USPS'>USPS</option>\n"
									. "</select>\n";
								}else {
									print $val;
									$shipper = $val; 							
								}
								print "</center></td>";
								break;
							case 'track_num':
								print "<td><center>";
								if(!$val) {
									print "<input type='text' name='" . $key . "' id='track_num' size='16' />";
								}else {
									switch($shipper) {
										case 'FedEx':
											print "<a href='http://www.fedex.com/Tracking?action=Track&cntry_code=us&tracknumbers=" . $val . "' target='_new'>" . $val . "</a>";
											break;
										case 'UPS':
											print "<a href='http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" . $val . "' target='_new'>" . $val . "</a>";
											break;
										case 'USPS':
											print "<a href='https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=" . $val . "' target='_new'>" . $val . "</a>";
											break;
										default:
											print $val;
											break;
										}
								}
								print "</center></td>";
								break;
								case 'mngr_notice_proc':
									print "<td><center>";
									if(!$val) {
										print "<span style='color: red;font-weight: bold;'>False</span>";
									}else {
										print "<span style='color: green;font-weight: bold;'>True</span>";
									}
									print "</center></td>";
									break;
								case 'arrive_date':
									print "<td nowrap>";
									if(!$val || $val == '0000-00-00') {
										print "<input type='text' name='" . $key . "' id='arrive_date' size='10' />";
									}else {
										print $val;
									}
									print "</td>";
									break;
								case 'arrive_rcvd':
									print "<td><center>";
									if(!$val) {
										print "<input type='text' size='4' name='" . $key . "' id='arrive_rcvd' />";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'contents_okay':
									print "<td><center>";
									if(!$val) {
										print "<input type='checkbox' name='" . $key . "' id='contents_okay' value='1' />";
									}else {
										print  "<span style='color: green;font-weight: bold;'>True</span>";
									}
									print "</center></td>";
									break;
								case 'on_location':
									print "<td><center>";
									if(!$val) {
										print "<input type='checkbox' name='" . $key . "' id='on_location' value='1' />";
									}else {
										print "<span style='color: green;font-weight: bold;'>True</span>";
									}
									print "</center></td>";
									break;
								case 'loc_rcvd':
									print "<td><center>";
									if(!$val) {
										print "<input type='text' size='4' name='" . $key . "' id='loc_rcvd' />";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'cus_notice_arrive':
									print "<td><center>";
									if(!$val) {
										print "<span style='color: red;font-weight: bold;'>False</span>";
									}else {
										print "<span style='color: green;font-weight: bold;'>True</span>";
									}
									print "</center></td>";
									break;
								case 'cus_follow_1':
									print "<td><center>";
									if(!$val) {
										print "<textarea rows='1' cols='4' name='" . $key . "' id='cus_follow_1'></textarea>";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'cus_follow_2':
									print "<td><center>";
									if(!$val) {
										print "<textarea rows='1' cols='4' name='" . $key . "' id='cus_follow_2'></textarea>";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'cus_follow_3':
									print "<td><center>";
									if(!$val) {
										print "<textarea rows='1' cols='4' name='" . $key . "' id='cus_follow_3'></textarea>";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'cus_pickup':
									print "<td nowrap>";
									if(!$val || $val == '0000-00-00') {
										print "<input type='text' name='" . $key . "' id='cus_pickup' size='10' />";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'pickup_emp':
									print "<td><center>";
									if(!$val) {
										print "<input type='text' name='" . $key . "' id='pickup_emp' size='4' />";
									}else {
										print $val;
									}
									print "</center></td>";
									break;
								case 'mngr_notice_pickup':
									print "<td><center>";
									if(!$val) {
										print "<span style='color: red;font-weight: bold;'>False</span>";
									}else {
										print "<span style='color: green;font-weight: bold;'>True</span>";
									}
									print "</center></td>";
									break;
								case 'age':
									if($val) {
										$age_complete = $val;
									}
									break;
								case 'complete':
									print "<td><center>";
									$datediff = time() - $start_date;
									$age = floor($datediff / '86400');
									if(!$val && $start_date != '0') {
										print "<span style='color: red;font-weight: bold;'>" . $age . "</span>";
									}elseif(!$val && $start_date == '0') {
										print "<span style='color: red;font-weight: bold;'>--</span>";
									}else {
										print "<span style='color: green;font-weight: bold;'>" . $age_complete . "</span>";
									}
									print "</center></td>";
									break;
							break;												
						}	
					}
			 print "</tr>";   
			    }
			 print "</table><input type='submit' value='Update values entered' /></fieldset></div>";
		  }
		  catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
		  }
		return $start_date;
	  }
	
	function proc_notify_mngr($db) {
		$count =0;
		try {
			$sql = "SELECT cfo_approve, process_date,mngr_notice_proc FROM actions WHERE order_id='" . $_POST['order_id']	. "'";
			foreach($db->query($sql) as $row) {
		    	 foreach($row as $key=>$val) {
		    	 	if($val && $val != '0000-00-00' && $key != 'mngr_notice_proc') {
		    	 		$count++;
		    	 		$notified = '0';
			    	 }elseif($key == 'mngr_notice_proc' && $val == '1') {
			    	 	$notified = '1';
			    }
			}
	    	 if($count == '2' && $notified == '0') {
	    	 	$pickupsql = "SELECT customers.pickup_loc FROM customers JOIN orders ON orders.cus_id = customers.cus_id WHERE orders.order_id = '$_POST[order_id]'";
				$stmt = $db->query($pickupsql);	    	 	
	    	 	$pickupresult = $stmt->fetch(PDO::FETCH_ASSOC);
	    	 	foreach($pickupresult as $key=>$val) {
		    	 	if($val == 'NARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}elseif($val == 'SARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}
			   }
			   $subject = 'An Order For Your Gym Has Been Processed';
			   $message = "Hello. This is an automated message. Please do not reply to this message - the inbox is not monitored.\n"
			   . "If you have questions please send a message to tommy@austinrockgym.com.\n\n"
			   . "Order #" . $_POST['order_id'] . " has been processed with the vendor.\n"
			   . "Please check the ARG Order Log for more details.\n\n";
			   $fromaddress = 'FROM: Austin Rock Gym Orders orders@austinrockgym.com';
	    	 	if (mail($toaddress, $subject, $message, $fromaddress, $additional_parameters = null)) {
	    	 	 	$sql = "UPDATE actions SET mngr_notice_proc='1' WHERE order_id='" .$_POST['order_id'] . "'";
		    	 	$db->exec($sql);
		    	 	print "<script>alert('Manager notified of processed order.');</script>";
		    	 }else {
					print "<script>alert('There was an error sending the Manager Notice. Please notify system admin.');</script>";	    	 
		    	 }
		     }
	    	 }	
			}
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
	   }
		
	}	
	
	function arrive_notify_mngr($db) {
		$count =0;
		try {
			$sql = "SELECT arrive_date, arrive_rcvd, mngr_notice_arrive FROM actions WHERE order_id='" . $_POST['order_id']	. "'";
			foreach($db->query($sql) as $row) {
		    	 foreach($row as $key=>$val) {
		    	 	if($val && $val != '0000-00-00' && $key != 'mngr_notice_arrive') {
		    	 		$count++;
		    	 		$notified = '0';
			    	 }elseif($key == 'mngr_notice_arrive' && $val == '1') {
			    	 	$notified = '1';
			    }
			}
	    	 if($count == '2' && $notified == '0') {
	    	 	$pickupsql = "SELECT customers.pickup_loc FROM customers JOIN orders ON orders.cus_id = customers.cus_id WHERE orders.order_id = '$_POST[order_id]'";
				$stmt = $db->query($pickupsql);	    	 	
	    	 	$pickupresult = $stmt->fetch(PDO::FETCH_ASSOC);
	    	 	foreach($pickupresult as $key=>$val) {
		    	 	if($val == 'NARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}elseif($val == 'SARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}
			   }
			   $subject = 'An Order For Your Gym Has Arrived';
			   $message = "Hello. This is an automated message. Please do not reply to this message - the inbox is not monitored.\n"
			   . "If you have questions please send a message to tommy@austinrockgym.com.\n\n"
			   . "Order #" . $_POST['order_id'] . " has arrived and will be transfered to your gym within 24 hours.\n"
			   . "Please check the ARG Order Log for more details.\n\n";
			   $fromaddress = 'FROM: Austin Rock Gym Orders orders@austinrockgym.com';
	    	 	if (mail($toaddress, $subject, $message, $fromaddress, $additional_parameters = null)) {
	    	 		$sql = "UPDATE actions SET mngr_notice_arrive ='1' WHERE order_id='$_POST[order_id]'";
	    	 		$db->exec($sql);
	    	 	 	print "<script>alert('Manager notified of order arrival');</script>";
		    	 }else {
					print "<script>alert('There was an error sending the Manager Notice. Please notify system admin.');</script>";	    	 
		    	 }
		     }
	    	 }	
			}
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
	   }
		
	}

	function onloc_notify_customer($db) {
		$count =0;
		try {
			$sql = "SELECT on_location, loc_rcvd, cus_notice_arrive FROM actions WHERE order_id='" . $_POST['order_id']	. "'";
			foreach($db->query($sql) as $row) {
		    	 foreach($row as $key=>$val) {
		    	 	if($val && $val != '0000-00-00' && $key != 'cus_notice_arrive') {
		    	 		$count++;
		    	 		$notified = '0';
			    	 }elseif($key == 'cus_notice_arrive' && $val) {
			    	 	$notified = '1';
			    	 }
			    }
			}
	    	 if($count == '2' && $notified == '0') {
	    	 	$pickupsql = "SELECT customers.email FROM customers JOIN orders ON orders.cus_id = customers.cus_id WHERE orders.order_id = '$_POST[order_id]'";
				$stmt = $db->query($pickupsql);	    	 	
	    	 	$pickupresult = $stmt->fetch(PDO::FETCH_ASSOC);
	    	 	foreach($pickupresult as $key=>$val) {
		    	 	if($val) {
		    	 		$toaddress = $val;
		    	 	}
			   }
			   $subject = 'Your Order From Austin Rock Gym Has Arrived!';

				$sql = "SELECT orders.vendor, orders.item, orders.cus_id, customers.firstname, customers.lastname, customers.pickup_loc FROM orders JOIN customers ON orders.cus_id = customers.cus_id WHERE orders.order_id ='$_POST[order_id]'";			   
			   foreach($db->query($sql) as $row) {
				   foreach($row as $key=>$val) {
					   switch($key) {
					   	case 'vendor':
						   	$vendor = $val;
						   	break;
					   	case 'item':
						   	$item = $val;
						   	break;
					   	case 'firstname':
						   	$first = $val;
						   	break;
					   	case 'lastname':
						   	$last = $val;
						   	break;
					   	case 'pickup_loc':
						   	$location = $val;
						   	break;
				   	break;
					   }
				   }
				}
			   $message = "Hello " . $first . ",\n\t"
			   . "Your recent order has arrived at " . $location . " and is ready for your pickup!\n\n"
			   . "The order details are: " . $first . " " . $last . "\n\t"
			   . "Order #: " . $_POST['order_id'] . " Contents: " . $vendor . " - " . $item . ".\n\n"
			   . "Please print this information and present it to an Austin Rock Gym employee to receive your product.\n\n"
			   . "If you have questions please contact the gym at (512) 416-9299 or info@austinrockgym.com.\n\n"
			   . "**Please do not reply to this email as the inbox is unmonitored and your message will not be received**";
			   $fromaddress = 'FROM: Austin Rock Gym Orders orders@austinrockgym.com';
	    	 	if (mail($toaddress, $subject, $message, $fromaddress, $additional_parameters = null)) {
	    	 		$sql = "UPDATE actions SET cus_notice_arrive ='" . date('Y-m-d') . "' WHERE order_id='$_POST[order_id]'";
	    	 		$db->exec($sql);
	    	 	 	print "<script>alert('Customer notified of order arrival on ". date('Y-m-d') . "');</script>";
		    	 }else {
					print "<script>alert('There was an error sending the Customer Notice. Please notify system admin.');</script>";	    	 
		    	 }
		     }
	    	 }	
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
	   }
		
	}
	
	function pickup_notify_mngr($db, $start) {
		$count =0;
		try {
			$sql = "SELECT cus_pickup, pickup_emp, mngr_notice_pickup FROM actions WHERE order_id='" . $_POST['order_id']	. "'";
			foreach($db->query($sql) as $row) {
		    	 foreach($row as $key=>$val) {
		    	 	if($val && $val != '0000-00-00' && $key != 'mngr_notice_pickup') {
		    	 		$count++;
		    	 		$notified = '0';
			    	 }elseif($key == 'mngr_notice_pickup' && $val == '1') {
			    	 	$notified = '1';
			    }
			}
	    	 if($count == '2' && $notified == '0') {
	    	 	$pickupsql = "SELECT customers.pickup_loc FROM customers JOIN orders ON orders.cus_id = customers.cus_id WHERE orders.order_id = '$_POST[order_id]'";
				$stmt = $db->query($pickupsql);	    	 	
	    	 	$pickupresult = $stmt->fetch(PDO::FETCH_ASSOC);
	    	 	foreach($pickupresult as $key=>$val) {
		    	 	if($val == 'NARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}elseif($val == 'SARG') {
		    	 		$toaddress = 'tommy@austinrockgym.com';
		    	 	}
			   }
			   $subject = 'An Order For Your Gym Has Been Picked Up';
			   $message = "Hello. This is an automated message. Please do not reply to this message - the inbox is not monitored.\n"
			   . "If you have questions please send a message to tommy@austinrockgym.com.\n\n"
			   . "Order #" . $_POST['order_id'] . " has been picked up by the customer. The order is now closed.\n"
			   . "Please check the ARG Order Log for more details.\n\n";
			   $fromaddress = 'FROM: Austin Rock Gym Orders orders@austinrockgym.com';
	    	 	if (mail($toaddress, $subject, $message, $fromaddress, $additional_parameters = null)) {
	    	 		
	    	 		$datediff = time() - $start;
					$age = floor($datediff / '86400');
	    	 		
	    	 		$sql = "UPDATE actions SET mngr_notice_pickup ='1', complete='1', age='$age' WHERE order_id='$_POST[order_id]'";
	    	 		$db->exec($sql);
	    	 	 	print "<script>alert('Manager notified of order pickup');</script>";
		    	 }else {
					print "<script>alert('There was an error sending the Manager Notice. Please notify system admin.');</script>";	    	 
		    	 }
		     }
	    	 }	
			}
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
	   }
		
	}
	
	function update_actions($db, $start) {
		try {
			foreach($_POST as $key=>$val)
			 {
			 	if($key != 'updated') {
					$sql = "UPDATE actions SET " . $key . "='" . $val . "' WHERE order_id='" . $_POST['order_id'] . "'";
					$db->exec($sql);
				}			 
			 }
			print "<script>alert('Order " . $_POST['order_id'] . " updated!');</script>";
			proc_notify_mngr($db);
			arrive_notify_mngr($db);
			onloc_notify_customer($db);
			pickup_notify_mngr($db, $start);
		}
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
	   }
	}  
	
	if(isset($_POST['updated']) == 'true') {
		 update_actions($db, $_SESSION['start']);
	}
	$_SESSION['start'] = generate_details($db);