<?php
    // First we execute our common code to connection to the database and start the session 
    require("menu.php");
    
    function list_active($db) {	
	echo "<fieldset class='wrapper'>\n"
	. "<legend class='button' onClick=toggle('active')>Active Users</legend>\n"
	. "<div class='hidden' id='active'>";
	$active_sql = "SELECT users.id, users.firstname, users.lastname, users.username, users.email, users.active, users.admin FROM users WHERE users.active = '1' ORDER BY users.admin DESC, users.firstname ASC";
	echo "<table class='imagetable'>\n"
	. "<tr><th>First Name</th><th>Last Name</th><th>Username</th><th>E-mail</th><th>Active</th><th>Admin</th></tr>\n";
	try {
	    foreach($db->query($active_sql) as $row) {
                echo "<tr>\n";
                foreach($row as $key=>$val) {
                    if ($key == 'id') {
                        $user = $val;
                    }
                    elseif ($key != 'id') {
                        echo "<td>\n";				
                        if($key == 'admin' && $val == '1') {
                            echo "<form name='setadmin' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();' checked=true>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='admin' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'admin' && $val != '1') {
                            echo "<form name='setadmin' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();'>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='admin' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'active' && $val == '1') {
                            echo "<form name='setactive' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();' checked=true>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='active' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'active' && $val != '1') {
                            echo "<form name='setactive' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();'>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='active' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        else {
                            echo $val;				
                        }
                    }
                    echo "</td>\n";
                }
                echo "<td>\n"
                . "<input type='button' class='button' onClick=toggle('reset-" . $user . "') value='Reset Password'>"
                . "<div class='hidden float-center' id='reset-" . $user . "'>\n"
                . "<form name='resetpass' action='register.php' method='post'>\n"
                . "<input type='hidden' name='user' value='" . $user . "'>\n"
                . "Password:&nbsp;<input type='password' name='password'><br /><br />\n"
                . "Confirm Password:&nbsp;<input type='password' name='confirm'><br />\n"
                . "<input type='submit' class='button'>\n"
                . "</form></div></td>\n";
                echo "</tr>\n";
	    }
	    echo "</table>\n"
            . "</div>\n"
            . "</fieldset><br />";
	}
	catch(PDOException $ex){
	    die("Unable to list active users.");
	}
    }
    
function list_inactive($db) {	
	echo "<fieldset class='wrapper'>\n"
	. "<legend class='button' onClick=toggle('inactive')>Inactive Users</legend>\n"
	. "<div class='hidden' id='inactive'>";
	$inactive_sql = "SELECT users.id, users.firstname, users.lastname, users.username, users.email, users.active, users.admin FROM users WHERE users.active = '0' ORDER BY users.firstname ASC";
	echo "<table class='imagetable'>\n"
	. "<tr><th>First Name</th><th>Last Name</th><th>Username</th><th>E-mail</th><th>Active</th><th>Admin</th></tr>\n";
	try {
	    foreach($db->query($inactive_sql) as $row) {
                echo "<tr>\n";
                foreach($row as $key=>$val) {
                    if ($key == 'id') {
                        $user = $val;
                    }
                    elseif ($key != 'id') {
                        echo "<td>\n";				
                        if($key == 'admin' && $val == '1') {
                            echo "<form name='setadmin' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();' checked=true>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='admin' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'admin' && $val != '1') {
                            echo "<form name='setadmin' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();'>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='admin' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'active' && $val == '1') {
                            echo "<form name='setactive' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();' checked=true>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='active' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        elseif($key == 'active' && $val != '1') {
                            echo "<form name='setactive' action='register.php' method='post'>\n"
                            . "<input type='checkbox' onClick='this.form.submit();'>\n"
                            . "<input type='hidden' name='user' value='".$user."'>\n"
                            . "<input type='hidden' name='active' value='" . $val . "'>\n"
                            . "</form>";
                        }
                        else {
                            echo $val;				
                        }
                    }
                    echo "</td>\n";
                }
                echo "<td>\n"
                    . "<input type='button' class='button' onClick=toggle('reset-" . $user . "') value='Reset Password'>"
                    . "<div class='hidden float-center' id='reset-" . $user . "'>\n"
                    . "<form name='resetpass' action='register.php' method='post'>\n"
                    . "<input type='hidden' name='user' value='" . $user . "'>\n"
                    . "Password:<br><input type='password' name='password'><br /><br />\n"
                    . "Confirm Password:<br><input type='password' name='confirm'><br /><br />\n"
                    . "<input type='submit' class='button'>\n"
                    . "</form></div></td>\n";
                    echo "</tr>\n";
	    }
	    echo "</table>\n"
            . "</div>\n"
            . "</fieldset><br />";
	}
	catch(PDOException $ex){
	    die("Unable to list inactive users.");
	}
    }    

function setadmin($details, $db) {
    try {        
        if ($_POST['admin'] == '0') {
            $set = '1';
        }
        else {
            $set = '0';
        }
	$sql = "UPDATE users SET admin = '$set' WHERE id = '$_POST[user]';";
	$result = $db->exec($sql);
	print "<script>alert('Admin status updated successfully!'); window.location = 'register.php';</script>";			
    }
    catch(PDOException $ex){
	die("Unable to update notes!");
    }	
}

function setactive($details, $db) {
    try {        
        if ($_POST['active'] == '0') {
            $set = '1';
        }
        else {
            $set = '0';
        }
	$sql = "UPDATE users SET active = '$set' WHERE id = '$_POST[user]';";
	$result = $db->exec($sql);
	print "<script>alert('Active status updated successfully!'); window.location = 'register.php';</script>";			
    }
    catch(PDOException $ex){
	die("Unable to active status!");
    }	
}

 function genpass($pass) {   
    // A salt is randomly generated here to protect again brute force attacks 
    // and rainbow table attacks.  The following statement generates a hex 
    // representation of an 8 byte salt.  Representing this in hex provides 
    // no additional security, but makes it easier for humans to read. 
    // For more information: 
    // http://en.wikipedia.org/wiki/Salt_%28cryptography%29 
    // http://en.wikipedia.org/wiki/Brute-force_attack 
    // http://en.wikipedia.org/wiki/Rainbow_table 
    $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
     
    // This hashes the password with the salt so that it can be stored securely 
    // in your database.  The output of this next statement is a 64 byte hex 
    // string representing the 32 byte sha256 hash of the password.  The original 
    // password cannot be recovered from the hash.  For more information: 
    // http://en.wikipedia.org/wiki/Cryptographic_hash_function 
    $password = hash('sha256', $_POST['password'] . $salt); 
     
    // Next we hash the hash value 65536 more times.  The purpose of this is to 
    // protect against brute force attacks.  Now an attacker must compute the hash 65537 
    // times for each guess they make against a password, whereas if the password 
    // were hashed only once the attacker would have been able to make 65537 different  
    // guesses in the same amount of time instead of only one. 
    for($round = 0; $round < 65536; $round++) { 
        $password = hash('sha256', $password . $salt); 
    }
    return array($password, $salt);
}

function changepass($details, $db) {
    $query = " 
        UPDATE users SET  
            password = :password, 
            salt = :salt
        WHERE id = :id;
    "; 
    list($password, $salt) = genpass($details['password']);     
    // Here we prepare our tokens for insertion into the SQL query.  We do not 
    // store the original password; only the hashed version of it.  We do store 
    // the salt (in its plaintext form; this is not a security risk). 
    $query_params = array(
        ':password' => $password, 
        ':salt' => $salt,
        ':id' => $details['user']
    ); 
     
    try { 
        // Execute the query to create the user 
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        // Note: On a production website, you should not output $ex->getMessage(). 
        // It may provide an attacker with helpful information about your code.  
        die("An error occurred. Please try again./n/t$query"); 
    } 
     
    // This redirects the user back to the login page after they register 
    //header("Location: login.php"); 
     
    // Calling die or exit after performing a redirect using the header function 
    // is critical.  The rest of your PHP script will continue to execute and 
    // will be sent to the user if you do not die or exit. 
    //die("Redirecting to login.php");
    echo "<script>alert('Password changed successfully!'); window.location = 'register.php';</script>";
}

     
function register($details,$db) {
    if(!empty($details)) { 
    // Ensure that the user has entered a non-empty username 
    if(empty($details['username'])) { 
        // Note that die() is generally a terrible way of handling user errors 
        // like this.  It is much better to display the error with the form 
        // and allow the user to correct their mistake.  However, that is an 
        // exercise for you to implement yourself. 
        die("Please enter a username."); 
    } 
     
    // Ensure that the user has entered a non-empty password 
    if(empty($details['password'])) { 
        die("Please enter a password."); 
    } 
     
    // Make sure the user entered a valid E-Mail address 
    // filter_var is a useful PHP function for validating form input, see: 
    // http://us.php.net/manual/en/function.filter-var.php 
    // http://us.php.net/manual/en/filter.filters.php 
    if(!filter_var($details['email'], FILTER_VALIDATE_EMAIL)) { 
        die("Invalid E-Mail Address"); 
    } 
     
    // We will use this SQL query to see whether the username entered by the 
    // user is already in use.  A SELECT query is used to retrieve data from the database. 
    // :username is a special token, we will substitute a real value in its place when 
    // we execute the query. 
    $query = " 
        SELECT 
            1 
        FROM users 
        WHERE 
            username = :username 
    "; 
     
    // This contains the definitions for any special tokens that we place in 
    // our SQL query.  In this case, we are defining a value for the token 
    // :username.  It is possible to insert $details['username'] directly into 
    // your $query string; however doing so is very insecure and opens your 
    // code up to SQL injection exploits.  Using tokens prevents this. 
    // For more information on SQL injections, see Wikipedia: 
    // http://en.wikipedia.org/wiki/SQL_Injection 
    $query_params = array( 
        ':username' => $details['username'] 
    ); 
     
    try { 
        // These two statements run the query against your database table. 
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        // Note: On a production website, you should not output $ex->getMessage(). 
        // It may provide an attacker with helpful information about your code.  
        die("An error occurred. Please try again."); 
    } 
     
    // The fetch() method returns an array representing the "next" row from 
    // the selected results, or false if there are no more rows to fetch. 
    $row = $stmt->fetch(); 
     
    // If a row was returned, then we know a matching username was found in 
    // the database already and we should not allow the user to continue. 
    if($row) { 
        die("This username is already in use"); 
    } 
     
    // Now we perform the same type of check for the email address, in order 
    // to ensure that it is unique. 
    $query = " 
        SELECT 
            1 
        FROM users 
        WHERE 
            email = :email 
    "; 
     
    $query_params = array( 
        ':email' => $details['email'] 
    ); 
     
    try { 
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        die("An error occurred. Please try again."); 
    } 
     
    $row = $stmt->fetch(); 
     
    if($row) { 
        die("This email address is already registered"); 
    } 
     
    // An INSERT query is used to add new rows to a database table. 
    // Again, we are using special tokens (technically called parameters) to 
    // protect against SQL injection attacks. 
    $query = " 
        INSERT INTO users ( 
            username, 
            password, 
            salt, 
            email,
            firstname,
            lastname,
            active,
            admin 
        ) VALUES ( 
            :username, 
            :password, 
            :salt, 
            :email,
            :first,
            :last,
            '1',
            :admin
        ) 
    "; 
    list($password, $salt) = genpass($details['password']);     
    // Here we prepare our tokens for insertion into the SQL query.  We do not 
    // store the original password; only the hashed version of it.  We do store 
    // the salt (in its plaintext form; this is not a security risk). 
    $query_params = array(
                    ':first' => $details['first'],
                    ':last' => $details['last'], 
        ':username' => $details['username'], 
        ':password' => $password, 
        ':salt' => $salt, 
        ':email' => $details['email'],
        ':admin' => $details['admin'] 
    ); 
     
    try { 
        // Execute the query to create the user 
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        // Note: On a production website, you should not output $ex->getMessage(). 
        // It may provide an attacker with helpful information about your code.  
        die("An error occurred. Please try again./n/t$query"); 
    } 
     
    // This redirects the user back to the login page after they register 
    //header("Location: login.php"); 
     
    // Calling die or exit after performing a redirect using the header function 
    // is critical.  The rest of your PHP script will continue to execute and 
    // will be sent to the user if you do not die or exit. 
    //die("Redirecting to login.php");
    echo "<script>alert('New user added successfully!'); window.location = 'register.php';</script>";			;
    }
}
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
	// If they are not, we redirect them to the login page. 
	header("Location: login.php"); 
	 
	// Remember that this die statement is absolutely critical.  Without it, 
	// people can view your members-only content without logging in. 
	die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
     
    // This if statement checks to determine whether the registration form has been submitted 
    // If it has, then the registration code is run, otherwise the form is displayed
    if($_SESSION['user']['admin'] == '1') {
        
        ?>
	<link rel='stylesheet' type='text/css' href='/css/customer.css'>
	<link rel='stylesheet' type='text/css' href='css/log.css'>
	<script type='text/javascript' src='scripts/notes.js'></script>
	<fieldset class='wrapper'>
	<legend class='button' onClick=toggle('register')>Register New User</legend>
	<div class='hidden' id='register'>
	<form action='register.php' method='post' onsubmit='return confirmPass()'>
	Firstname:
	<input type='text' name='first' value='' />
	Lastname:
	<input type='text' name='last' value='' />
	<br /><br />
	E-Mail:
	<input type='email' name='email' value='' />
	<br /><br />
        Username:
        <input type='text' name='username' value='' />
	<br /><br />
	Password:
	<input type='password' id='pass' name='password' value='' />
	Confirm Password:
	<input type='password' id='confirm' name='confirm' value='' />
	<br /><br />
	<input type='hidden' name='admin' value='0' />
	Make admin
	<input type='checkbox' name='admin' value='1'/>
	<br /><br />
        <input type='hidden' id='register' name='register' value='1'>
	<input type='submit' value='Register' />
	</form>
	</div>
	</fieldset><br />

        <?
   
        if (!empty($_POST['user'])) {
            if ($_POST['active'] != null) {
                setactive($_POST, $db);
            }
            if ($_POST['admin'] != null) {
                setadmin($_POST, $db);
            }
            if ($_POST['password'] != null) {
                changepass($_POST, $db);
            }
        }
        if ($_POST['register'] == '1') {
                register($_POST, $db);
            }
        
        list_active($db);
        list_inactive($db);
}