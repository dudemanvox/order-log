function setGym(){
var x = document.getElementById('cusType').value;
if (x=='1' || x=='2') {
 document.getElementById('last').value = "Retail";
 if (x == '2') {
	 document.getElementById('first').value = "SARG";
	 document.getElementById('phone').value = "5124412423";
	 document.getElementById('email').value = "sarg@austinrockgym.com";
	 document.getElementById('location').value = "SARG";
 }
 if (x == '1') {
	 document.getElementById('first').value = "NARG";
	 document.getElementById('phone').value = "5124169299";
	 document.getElementById('email').value = "narg@austinrockgym.com";
	 document.getElementById('location').value = "NARG";
}
}
}
function clearChildren(element) {
 for (var i = 0; i < element.childNodes.length; i++) {
  var e = element.childNodes[i];
  if (e.tagName) switch (e.tagName.toLowerCase()) {
     case 'input':
        switch (e.type) {
         case "radio":
         case "checkbox": e.checked = false; break;
         case "button":
         case "submit":
         case "image": break;
         default: e.value = ''; break;
        }
      break;
      case 'select': e.selectedIndex = 0; break;
      case 'textarea': e.innerHTML = ''; break;
      default: clearChildren(e);
     }
  }
}
function cusSelect () {
 var x = document.getElementById('cusType').value;
 if(x == '3' || x == '4'){
	document.getElementById('customer').style.display = "block"; 
 }else{
  document.getElementById('customer').style.display = "none";
  clearChildren(document.getElementById('customer'));
  }
}
function validateName(x){
 var re = /[A-Za-z-']$/;
 if(re.test(document.getElementById(x).value)){
  document.getElementById(x).style.background ='#ccffcc';
  document.getElementById(x + 'Error').style.display = "none";
  return true;
  }else{
   document.getElementById(x).style.background ='#e35152';
   document.getElementById(x + 'Error').style.display = "block";
   return false; 
  }
 }
function validatePhone(x){
 var re = /^\d{10}$/;
 if (re.test(document.getElementById(x).value)){
  document.getElementById(x).style.background = '#ccffcc';
  document.getElementById(x + 'Error').style.display = "none";
  return true; 
 }else{
  document.getElementById(x).style.background = '#e35152';
  document.getElementById(x + 'Error').style.display = "block";
  return false;  
 }
}
function validateEmail(x){ 
 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 if(re.test(document.getElementById(x).value)){
  document.getElementById(x).style.background ='#ccffcc';
  document.getElementById(x + 'Error').style.display = "none";
  return true;
 }else{
  document.getElementById(x).style.background ='#e35152';
  document.getElementById(x + 'Error').style.display ="block";
  return false;
 }
}
function validateLoc(x){
  if(document.getElementById(x).selectedIndex !== 0){
    document.getElementById(x).style.background ='#ccffcc';
    document.getElementById(x + 'Error').style.display = "none";
    return true;
  }else{
    document.getElementById(x).style.background ='#e35152';
    document.getElementById(x + 'Error').style.display = "block";
    return false; 
  }
}

function validateAlpha(x){
 var re = /^[-\sa-zA-Z]+$/;
 if (re.test(document.getElementById(x).value)) {
 	document.getElementById(x).style.background = '#ccffcc';
 	document.getElementById(x + 'Error').style.display = "none";
 	return true;
 }else {
	document.getElementById(x).style.background = '#e35152';
	document.getElementById(x + 'Error').style.display = "block";
	return false;
 }
}

function validateAlphaNumeric(x) {
 var re = /^[\w\.\s]+$/;
 if (re.test(document.getElementById(x).value)) {
 	document.getElementById(x).style.background = '#ccffcc';
 	document.getElementById(x + 'Error').style.display = "none";
 	return true;
 }else {
	document.getElementById(x).style.background = '#e35152';
	document.getElementById(x + 'Error').style.display = "block";
	return false;
 }	
}

function validateNumeric(x) {
 var re = /^\d+$/;
 if (re.test(document.getElementById(x).value)) {
 	document.getElementById(x).style.background = '#ccffcc';
 	document.getElementById(x + 'Error').style.display = "none";
 	return true;
 }else {
	document.getElementById(x).style.background = '#e35152';
	document.getElementById(x + 'Error').style.display = "block";
	return false;
 }		
}

function validatePrice(x) {
 var re = /^\d+([\.]\d+)?$/;
 if (re.test(document.getElementById(x).value)) {
 	document.getElementById(x).style.background = '#ccffcc';
 	document.getElementById(x + 'Error').style.display = "none";
 	return true;
 }else {
	document.getElementById(x).style.background = '#e35152';
	document.getElementById(x + 'Error').style.display = "block";
	return false;
 }			
}

function validateConf(x) {
 var re = /^(MC|mc|V|v|D|d)\-([\w\-.\s]{6})+$/;
 if (re.test(document.getElementById(x).value)) {
 	document.getElementById(x).style.background = '#ccffcc';
 	document.getElementById(x + 'Error').style.display = "none";
 	return true;
 }else {
	document.getElementById(x).style.background = '#e35152';
	document.getElementById(x + 'Error').style.display = "block";
	return false;
 }			
}

function validateForm(){
 var error = 0;
 if (!validateAlpha('vendor')) {
  document.getElementById('vendorError').style.display = "block";
  error++;
 }
 if (!validateAlpha('item')) {
  document.getElementById('itemError').style.display = "block";
  error++;
 }
 if (!validateAlphaNumeric('details')) { 
  document.getElementById('detailsError').style.display = "block";
  error++;
 }
 if (!validateNumeric('quantity')) {
  document.getElementById('quantityError').style.display = "block";
  error++;
 }
 if (!validatePrice('price')) {
  document.getElementById('priceError').style.display = "block";
  error++;
 }
 if (!validateConf('conf')) {
  document.getElementById('confError').style.display = "block";
  error++;
 }
 if (!validatePrice('shipping')) {
  document.getElementById('shippingError').style.display = "block";
  error++;
 }
 if(!validateName('first')){
  document.getElementById('firstError').style.display = "block";
  error++;
 }
 if (!validateName('last')) {
	document.getElementById('firstError').style.display = "block";
	error++;
 }
 if (!validatePhone('phone')) {
	document.getElementById('phoneError').style.display = "block";
	error++;
 }
 if (!validateEmail('email')) {
	document.getElementById('emailError').style.display = "block";
	error++;
 }
 if (!validateLoc('location')) {
	document.getElementById('locationError').style.display = "block";
	error++;
 }
// Don't submit form if there are errors
 if(error > 0){
  if (error == '1') {
	alert("There is an error. Please correct the red box and resubmit.");
  }else{  
  alert("There are "+error+" errors. Please correct the red boxes and resubmit.");
  } 
  return false;
 }
}