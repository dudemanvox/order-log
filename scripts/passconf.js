function confirmPass() {
    var pass = document.getElementById('pass'),
        confirm = document.getElementById('confirm'),
    if (pass.value != confirm.value) {
        alert('Passwords do not match!');
        pass.style.background = '#e35152';
        confirm.style.background = '#e35152';
        return false;
    } else {
        return true;
    }
}