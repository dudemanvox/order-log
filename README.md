# README #

Order Log requires a webserver with PHP and MySQL server. 
To install:
   1. Copy source files to target directory on webserver.
   2. Create database and authorized user.
   3. Create tables.
   4. Update the host, db, user, password in common.php and save changes.

### What is this repository for? ###

* ARG Order Log - Tracks orders placed for the gym. Order types are currently (NARG, SARG, NARG Customer, SARG Customer.
* Version 1.0

### Contact ###
Tommy Bregar
tommy@angryforcode.com