<?php
	// First we execute our common code to connection to the database and start the session 
	require("common.php"); 
	// At the top of the page we check to see whether the user is logged in or not 
	if(empty($_SESSION['user'])) 
	{ 
	// If they are not, we redirect them to the login page. 
	header("Location: login.php"); 
	// Remember that this die statement is absolutely critical.  Without it, 
	// people can view your members-only content without logging in. 
	die("Redirecting to login.php"); 
	} 
	// Everything below this point in the file is secured by the login system 
	// We can display the user's username to them by reading it from the session array.  Remember that because 
	// a username is user submitted content we must use htmlentities on it before displaying it to the user.
	ob_start();
	
	try{	
		if($_POST['order'] && $_POST['note']){
			$sql_note = "UPDATE orders SET notes = ". $db->quote($_POST['note']) . " WHERE order_id = '$_POST[order]';";
			$result = $db->exec($sql_note);
			print "<script>alert('Notes updated successfully! Returning to Order Log...'); window.location = 'log.php';</script>";			
		}else{
			print "Missing Data. Returning to Order Log!";
			sleep(5);
			while (ob_get_status()) 
				{
				    ob_end_clean();
				}
			header("Location: http://orders.austinrockgym.com/log.php");
			exit();		
			}
	}catch(PDOException $ex){
				die("Unable to update notes!" . $ex->getMessage());
			}