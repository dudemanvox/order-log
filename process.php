<?php
	// First we execute our common code to connection to the database and start the session 
	require("common.php"); 
	// At the top of the page we check to see whether the user is logged in or not 
	if(empty($_SESSION['user'])) 
	{ 
	// If they are not, we redirect them to the login page. 
	header("Location: login.php"); 
	// Remember that this die statement is absolutely critical.  Without it, 
	// people can view your members-only content without logging in. 
	die("Redirecting to login.php"); 
	} 
	// Everything below this point in the file is secured by the login system 
	// We can display the user's username to them by reading it from the session array.  Remember that because 
	// a username is user submitted content we must use htmlentities on it before displaying it to the user.
	
	function add_customer($db) {
		//Formats results of form.
		$_POST['phone'] = "(".substr($_POST['phone'], 0, 3).") ".substr($_POST['phone'], 3, 3)."-".substr($_POST['phone'],6);
		$_POST['first'] = ucfirst(strtolower($_POST['first']));
		$_POST['last'] = ucfirst(strtolower($_POST['last']));
		//Checks to see if the customer already exists.
		try{
			$check_sql = "SELECT * FROM customers WHERE firstname LIKE '$_POST[first]' AND lastname LIKE '$_POST[last]' AND phone LIKE '$_POST[phone]' AND email LIKE '$_POST[email]' AND pickup_loc LIKE '$_POST[location]'";
			$check_stmt = $db->prepare($check_sql);
			$count = '0';  
			foreach ($db->query($check_sql) as $row) {
				$count++ ;
			}
		}
		catch(PDOException $ex){
			die("Failed to execute query:" . $ex->getMessage());
		}
		//If customer exists, assigns the order to them by calling processOrder($cus_id).
		if($count > 0) {
			echo "Duplicate customer. Assigning order to existing customer...";
			process_order($row['cus_id'],$db);
		}
		//If customer does not exist, inserts new row into customers table and retrieves the new $cus_id, then calls process_order($cus_id, $db).
		elseif($count == '0') {
			$addnew_sql="INSERT INTO customers (firstname,lastname,phone,email,pickup_loc)
			VALUES
			('$_POST[first]','$_POST[last]','$_POST[phone]','$_POST[email]','$_POST[location]')";
			try{
				$result = $db->exec($addnew_sql);
				if($result == '1') {
					echo "New customer added!";
					$sql = "SELECT cus_id FROM customers ORDER BY cus_id DESC LIMIT 1";
					$result = $db->query($sql);
					$row = $result->fetch(PDO::FETCH_ASSOC);
					$cus_id = $row['cus_id'];
					process_order($cus_id, $db);
				} 
			}
			catch(PDOException $ex){
				die("Unable to add new customer");
			}
		}
	}

	function process_order($cus_id, $db) {
		$user_id = htmlentities($_SESSION['user']['id'], ENT_QUOTES, 'UTF-8');
		$insert_order = "INSERT INTO orders (vendor, item, detail, quantity, order_date, user_id, cus_id) VALUES	('$_POST[vendor]','$_POST[item]','$_POST[details]','$_POST[quantity]','$_POST[date]','$user_id','$cus_id')";
		$get_num = "SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1";
		try{
			$order_result = $db->exec($insert_order);
			$get_num_result = $db->query($get_num);
			$order_row = $get_num_result->fetch(PDO::FETCH_ASSOC);
			$order_id = $order_row['order_id'];
			$insert_action = "INSERT INTO actions (order_id, pmt_conf, cost) VALUES ('$order_id','" . strtoupper($_POST['conf']) . "','$_POST[price]')";
			$insert_result = $db->exec($insert_action);
		}
		catch(PDOException $ex) {
			die("Order could not be processed.");
		}
		if($order_result > 0 && $insert_result > 0) {
			echo '<script>
					alert("Order processed successfully!\nYou can now view it\'s details on the log page.");
					window.location.href="addcustomer.php";
					</script>';
		}
	}
	add_customer($db);
	
?>